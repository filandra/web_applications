<?php

/*
 * Create an array primes from 2 to $max.
 */
function sieve($max)
{
    $primes = [];
    $found = TRUE;
    
    if($max < 2){
        return [];
    }
    
	foreach (range(2, $max) as $number) {
	    $found = TRUE;
	    // Go through each smaller prime number
        foreach ($primes as $p) {
            // If the number is divisible, break and refuse
            if (($number % $p) == 0){
                $found = FALSE;
                break;
            }
        }
        
        if ($found){
            $primes[] = $number;
        }
    }
    
    return $primes;
}
