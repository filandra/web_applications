<?php

class ConfigPreprocessor
{
	private $root;
	private $extraction = [];
	private $counter = 0;	
	private $sorted_tasks = [];	

	public function __construct($config)
	{		
		$this->root = $config;
	}

	/**
	 * Get an array of tasks from the config in the right order.
	 */
	public function getAllTasks()
	{
		$this->recursiveWalk($this->root);

		// Sort by priority and place the tasks
		$priority_sorted = $this->extraction;
		usort($priority_sorted, function($a, $b)
		{
			if($a->priority == $b->priority){
				return $a->tree_priority > $b->tree_priority;
			}else{
				return $a->priority < $b->priority;
			}		    
		});		

		// Try to place tasks
		while(count($this->extraction) > 0){
			$before = count($this->extraction);
			
			// If we place a task, start again
			foreach ($priority_sorted as $key => $value) {
				$this->placeTask($value);

				if($before > count($this->extraction)){
					break;
				}
			}

			// If we try to place every task but fail every time, there is a cycle
			if($before == count($this->extraction)){
				throw new Exception('Cycle detected');
			}
		}

		$result = [];
		foreach ($this->sorted_tasks as $key => $value) {
			$result[] = $value->task;
		}

		return $result;
	}

	// Check every property or key of a node
	// If the value is a object or array duck type it and go deeper in recursion
	private function recursiveWalk($node){
		foreach ($node as $key => $value) {
			if (is_array($value)){
				$this->duckTypeArray($value);
				$this->recursiveWalk($value);
			}

			if (is_object($value)){
				$this->duckTypeObject($value);
				$this->recursiveWalk($value);
			}
		}
	}

	// Check if array has the right keys
	private function duckTypeArray($array){		
		if (array_key_exists('id', $array) and array_key_exists('command', $array) and array_key_exists('priority', $array) and array_key_exists('dependencies', $array)){
			$task = new Task($array, $array['id'], $array['dependencies'], $array['priority'], $this->counter);
			$this->extraction[$array['id']] = $task;
			$this->counter++;
		}
	}

	// Check if object has the right properties
	private function duckTypeObject($object){
		if (property_exists($object, "id") and property_exists($object, "command") and property_exists($object, "priority") and property_exists($object, "dependencies")){
			$task = new Task($object, $object->id, $object->dependencies, $object->priority, $this->counter);
			$this->extraction[$object->id] = $task;
			$this->counter++;
		}
	}

	private function placeTask($task){

		// Check if this node is not placed already
		if(!array_key_exists($task->id, $this->extraction)){
			return;
		}

		foreach ($task->dependencies as $key => $value) {
			// Wait until all dependencies are placed
			if(array_key_exists($value, $this->extraction)){
				return;
			}
		}
		
		// place the task in to correct position
		$index = 0;
		$this->dependenciesBottom($index, $task);
		$this->priorityBottom($index, $task);
		$this->tree_priorityBottom($index, $task);

		// If the index is out of the array of sorted tasks, append it
		if($index == count($this->sorted_tasks)){
			$this->sorted_tasks[] = clone $task;
		}else{			
			// If the index is somewhere inside of the array, insert it
			array_splice($this->sorted_tasks, $index, 0, [clone $task]);
		}

		// remove task extraction after succesful placement	
		unset($this->extraction[$task->id]);
	}

	private function dependenciesBottom(&$index, $task){
		$seen_ids = [];

		// Iterate through the sorted array		
		while($index != count($this->sorted_tasks) and $this->needDependencies($seen_ids, $this->sorted_tasks[$index]->id, $task->dependencies)){
			$index++;
		}

		// Check last index position - if we still need the another dependency, then error (dependency needed which is not available)
		if($this->needDependencies($seen_ids, "", $task->dependencies)){
			throw new Exception('Dependency missing');
		}
	}

	private function needDependencies(&$seen_ids, $id, $dependencies){
		foreach ($dependencies as $key => $value) {
			if (!in_array($value, $seen_ids)) {
				// We need to go deeper - add id from this position
				$seen_ids[] = $id;
				return True;
			}
		}
		// We are under all needed dependencies
		return False;
	}

	private function priorityBottom(&$index, $task){
		if (empty($this->sorted_tasks)) {
     		return;
		}

		// Iterate until we reach the end or lower priority
		while($index != count($this->sorted_tasks) and $this->sorted_tasks[$index]->priority > $task->priority){
			$index++;
		}
	}

	private function tree_priorityBottom(&$index, $task){
		if (empty($this->sorted_tasks)) {
     		return;
		}

		// Iterate until we reach the end or greater tree_priority (while still having lower or the same priority)
		while($index != count($this->sorted_tasks) and ($this->sorted_tasks[$index]->priority >= $task->priority or ($this->sorted_tasks[$index]->priority == $task->priority and $this->sorted_tasks[$index]->tree_priority < $task->tree_priority))){
			$index++;
		}
	}
}

class Task{
	public $task;
	public $id;
	public $dependencies;
	public $priority;
	public $tree_priority;	

	function __construct($task, $id, $dependencies, $priority, $tree_priority) {
        $this->task = $task;
        $this->id = $id;
        $this->dependencies = $dependencies;
        $this->priority = $priority;
        $this->tree_priority = $tree_priority;
    }
}