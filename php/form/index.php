<?php
    $path = explode('/', __DIR__);
    define("COMMENTS_FILE", "/board/" . $path[2] . "-comments.txt");

    // ignore for now
    session_start();
    setcookie("session_id", session_id());

    function validateInputField($comment) {
        return strlen($comment) > 0 && strlen($comment) <= 1000;
    }

    function storeComment($comment) {
        file_put_contents(COMMENTS_FILE, $comment . "\n", FILE_APPEND | LOCK_EX);
    }

    function sanitizeForHtml($comment) {
        return htmlspecialchars($comment);
    }

    function loadComments() {
        return file(COMMENTS_FILE);
    }

    function redirect($message) {
        header("Location: index.php?message=" . urlencode($message));
    }

    if (isset($_POST['comment'])) {
        $comment = $_POST['comment'];
        if (validateInputField($comment)) {
            foreach ($_POST['tag'] as $tag) {
                if (validateInputField($tag)) {
                    $comment = "$comment [$tag]";
                }
            }
            storeComment($comment);
            redirect("Message was submitted & stored");
        } else {
            redirect("Invalid message");
        }
    }


?><!DOCTYPE html>
<html>
<head>
    <title>Discussion board</title>
    <style>
        #root {
            max-width: 600px;
            margin: 0 auto;
        }
        input[type=text] {
            width: 100%;
            line-height:2em;
        }
        #comments {
            margin-top: 30px;
        }
        input[type=submit] {
            background-color:lightgreen;
            width: 50%;
            display:block;
            margin: 10px auto;
            padding: 10px;
        }
        input.tag {
            width: 15%;
            margin: 0 10px;
        }
    </style>
    <script>
        function addTag() {
            this.parentNode.insertBefore(document.getElementsByClassName('tag')[0].cloneNode(), this);
        }
        window.onload = function() {
            document.getElementById("add-tag").addEventListener('click', addTag);
        };
    </script>
</head>
<body>
<div id="root">
<h1>Discussion board</h1>

<p><?php
    if (isset($_GET['message'])) {
        echo sanitizeForHtml($_GET['message']);
    }
?></p>

<form action="index.php" method="POST" id="comment-form">
    <input type="text" name="comment" id="comment" />
    <p>Tags:
    <input class="tag" type="text" name="tag[]" />
    <input type="button" id="add-tag" value="Add"/>
    </p>
    <p><input type="submit" value="Post comment"/></p>
</form>

<ul id="comments">
<?php
    foreach (loadComments() as $comment) {
        echo "<li>" . sanitizeForHtml($comment) . "</li>";
    }
?>
</ul>

</div>
