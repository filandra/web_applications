<?php
    // Connect to database
    require __DIR__ . '/db_config.php'; 

    function validate_item($input) {        
        return strlen($input) > 0 && strlen($input) <= 100 && ctype_alpha($input);
    }

    function validate_amount($input) {        
        return strlen($input) > 0 && strlen($input) <= 100 && is_numeric($input) && intval($input) > 0;
    }

    $con = new mysqli($db_config['server'], $db_config['login'], $db_config['password'], $db_config['database']);
    if ($con->connect_error) {
        die("Could not connect to the database");
    }

    $sql = "";

    if (isset($_POST['edit_item']) and isset($_POST['edit_amount'])) {
        $item = $_POST['edit_item'];
        $amount = $_POST['edit_amount'];

        // Check input (sql-injection prevention)
        if(!validate_item($item) or !validate_amount($amount)){
            http_response_code(404);        
            die();
        }

        $sql = 'UPDATE list SET list.amount = "' . $con->real_escape_string($amount) . '" WHERE list.item_id IN (SELECT items.id FROM items WHERE items.name = "' . $con->real_escape_string($item) . '")';
    }

    if (isset($_POST['delete_item'])) {    
        $item = $_POST['delete_item'];

        // Check input (sql-injection prevention)
        if(!validate_item($item)){
            http_response_code(404);        
            die();
        }

        // lower the position of lower items
        $sql = 'SELECT position FROM list WHERE list.item_id IN (SELECT items.id FROM items WHERE items.name = "' . $con->real_escape_string($item) . '")';
        $result = mysqli_query($con, $sql) or (http_response_code(404) and die()); 
        $position = mysqli_fetch_array($result)["position"];
        
        $sql = 'UPDATE list SET list.position = list.position - 1 WHERE list.position > "' . $con->real_escape_string($position) . '"';
        $result = mysqli_query($con, $sql) or (http_response_code(404) and die()); 

        $sql = 'DELETE FROM list WHERE list.item_id IN (SELECT items.id FROM items WHERE items.name = "' . $con->real_escape_string($item) . '")';
    }

    if (isset($_POST['swap_item'])) {
        // First check the input (sql-injection prevention)
        if(!is_numeric($_POST['swap_item'])){
            http_response_code(404);        
            die();
        }

        $num = intval($_POST['swap_item']) + 1;
        $next = intval($num) + 1;

        // Check again for positivity (sql-injection prevention)
        if($num <=0 or $next <=0){
            http_response_code(404);        
            die();
        }

        $sql = 'UPDATE list SET list.position=-1 WHERE list.position=' . $num . '; UPDATE list SET list.position=-2 WHERE list.position=' . $next . '; UPDATE list SET list.position=' . $next . ' WHERE list.position=-1; UPDATE list SET list.position=' . $num . ' WHERE list.position=-2';
    }

    if($sql!=""){
        if(strpos($sql, ";") !== false){
            $result = mysqli_multi_query($con, $sql) or http_response_code(404);     
        }else{
            $result = mysqli_query($con, $sql) or http_response_code(404); 
        }                
    }else{
        http_response_code(404);        
        die();
    }
?>