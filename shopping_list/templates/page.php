<!DOCTYPE html>
<html>
<head>
    <title>Shopping List</title>
    <script src="controlls.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="style.css">	
</head>
<body>
<div id="page">
<h1>Shopping List</h1>

<p><?php
    if (isset($_GET['message'])) {
        echo sanitize($_GET['message']);
    }
        
    include __DIR__ . '/list.php';
?></p>

<br><br>

<h2>Add Item</h2>
<?php include __DIR__ . '/form.php'; ?>

<br><br>

</div>
</body>