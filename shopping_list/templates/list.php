<table>
	<thead>
		<tr>
			<th>Item</th>
			<th>Amount</th>			
		</tr>
	</thead>
	<tbody id="list">
		<?php
			$counter = 0;
			$result = loadRows();
	        while ($row = mysqli_fetch_array($result)) {
		        $item = $row["name"];
		        $amount = $row["amount"];
		        include __DIR__ . '/row.php';
		        $counter++;
        	}
		?>
	</tbody>
</table>