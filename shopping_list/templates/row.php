<?php
echo "<tr id=\"row-" . $counter . "\">";
echo "<td id=\"item-" . $counter . "\">" . sanitize($item) . "</td>";	
echo "<td id=\"amount-" . $counter . "\">" . sanitize($amount) . "</td>";	
echo "<td><button class=\"button swap\" id=\"swap-" . $counter . "\">⇵</button></td>";
echo "<td><button class=\"button edit\" id=\"edit-" . $counter . "\">Edit</button></td>";
echo "<td><button class=\"button delete\" id=\"delete-" . $counter . "\">Delete</button></td>";
echo "</tr>";
?>
