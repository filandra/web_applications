// Wait for the DOM to load
document.addEventListener("DOMContentLoaded", main);

function main(){
	add_listener();
}

function add_listener(){	
	// needed this way in order for innerHTML manipulation to work
	document.addEventListener('click',function(e){
    if(e.target && e.target.id.includes("swap")){
    	click_swap(e.target);
	 }

	if(e.target && e.target.id.includes("edit")){
    	click_edit(e.target);
	 }

	if(e.target && e.target.id.includes("delete")){
    	click_delete(e.target);
	 }})	
}

function setup_ajax(){
	xmlhttp = new XMLHttpRequest();
	xmlhttp.open("POST","database/database_controller.php",true);	
	xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	return xmlhttp;
}

function click_swap(target){
	var clicked_num = get_number(target.id);
	if (clicked_num >= document.getElementById("list").children.length - 1){		
		return;
	}	
	var next_num = clicked_num + 1;	

	xmlhttp = setup_ajax();
	xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4) {
            	if (this.status == 200) {
            		// Modify the row
                	swap_rows(clicked_num, next_num);
		        } else {
		        	window.alert("Database error");
		        }
            }
        };    
    xmlhttp.send("swap_item="+clicked_num);    
}

function swap_rows(clicked_num, next_num){	
	// swap rows
	var clicked_row = "row-" + clicked_num;	
	var next_row = "row-" + next_num;

	var clicked_element = document.getElementById(clicked_row);
	var next_element = document.getElementById(next_row);
	var temp = clicked_element.innerHTML;		

	clicked_element.innerHTML = next_element.innerHTML;	
	next_element.innerHTML = temp;		

	// swap ids
	var ids = ["swap-", "edit-", "delete-", "item-", "amount-"];
	for (var i = 0; i < ids.length; i++) {
		id = ids[i];
  		var clicked_element = document.getElementById(id+clicked_num);
		var next_element = document.getElementById(id+next_num);

		clicked_element.id = id+next_num;	
		next_element.id = id+clicked_num;
	}
}

function click_edit(target){	
	var clicked_num = get_number(target.id);
	var item = document.getElementById("item-"+clicked_num).innerHTML;
	var amount = prompt("Edit "+item+" to", "");	
	if (isNaN(amount) == true || Number(amount) <= 0) {
	  	window.alert("Invalid input!");
	  	return;
	}

	if(amount == null || amount == ""){
		return;
	}

	xmlhttp = setup_ajax();
	xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4) {
            	if (this.status == 200) {
            		// Modify the row
                	document.getElementById("amount-"+clicked_num).innerHTML = amount;
		        } else {
		        	window.alert("Database error");
		        }            	
            }
        };    
    xmlhttp.send("edit_item="+item+"&edit_amount="+amount);      
}

function click_delete(target){	
	var clicked_num = get_number(target.id);
	var clicked_item = document.getElementById("item-"+clicked_num).innerHTML;		

	xmlhttp = setup_ajax();
	xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4) {
            	if (this.status == 200) {            		
                	
                	// Decrement higher positions
            		var row_num = document.getElementById("list").children.length;
					for (var i = clicked_num+1; i < row_num; i++) {						
						decrement_position(i);
					}					

					// Delete the row from list
                	document.getElementById("row-"+clicked_num).remove();

		        } else {
		        	window.alert("Database error");
		        }
            }
        };
    xmlhttp.send("delete_item="+clicked_item);    
}

function decrement_position(position){
	var new_position = Number(position) - 1;	

	// decrement all relevant ids
	var ids = ["row-", "swap-", "edit-", "delete-", "item-", "amount-"];
	for (var i = 0; i < ids.length; i++) {
		id = ids[i];
  		var element = document.getElementById(id+position);  		
		element.id = id+new_position;		
	}		
}

function get_number(string){
	return Number(string.replace(/[a-z]/gi, "").replace(/\-/g, "")); 
}
