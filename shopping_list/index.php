<?php    
    session_start();
    setcookie("session_id", session_id());

    // Connect to database
    require __DIR__ . '/database/db_config.php';
    $con = new mysqli($db_config['server'], $db_config['login'], $db_config['password'], $db_config['database']);
    if ($con->connect_error) {
        die("Could not connect to the database");
    }

    function validate_item($input) {
        return strlen($input) > 0 && strlen($input) <= 100 && ctype_alpha($input);
    }

    function validate_amount($input) {        
        return strlen($input) > 0 && strlen($input) <= 100 && is_numeric($input) && intval($input) > 0;
    }

    function sanitize($comment) {    	
        return htmlspecialchars($comment);
    }

    function storeRow($item, $amount) {
        global $con;        
        $sql = 'SELECT * FROM items WHERE items.name = "' . $con->real_escape_string($item) . '"';
		$items_result = mysqli_query($con, $sql) or redirect("Database Error");

		if (is_null(mysqli_fetch_array($items_result))){		
			// item is not in the items table - add it
			$sql = 'SELECT MAX(id) FROM items';
			$max_id = mysqli_query($con, $sql) or redirect("Database Error");
			$next_id_num = mysqli_fetch_array($max_id)["MAX(id)"] + 1;

			$sql = 'INSERT INTO items VALUES ("' . $con->real_escape_string($next_id_num) . '", "' . $con->real_escape_string($item) . '")';
			$items_added = mysqli_query($con, $sql) or redirect("Database Error");
		}

		// The item is already in items table
		$sql = 'SELECT * FROM items WHERE items.name = "' . $con->real_escape_string($item) . '"';
		$items_result = mysqli_query($con, $sql) or redirect("Database Error");
		$id = mysqli_fetch_array($items_result)["id"];

		$sql = 'SELECT * FROM list WHERE list.item_id = "' . $con->real_escape_string($id) . '"';
		$list_result = mysqli_query($con, $sql) or redirect("Database Error");

		if(is_null(mysqli_fetch_array($list_result))){
			// item is not in the list - add it
			$sql = 'SELECT MAX(position) FROM list';
			$max_position = mysqli_query($con, $sql) or redirect("Error - ".mysqli_error($con));
			$next_position_num = mysqli_fetch_array($max_position)["MAX(position)"] + 1;

			$sql = 'INSERT INTO list VALUES ("' . $con->real_escape_string($id) . '", "' . $con->real_escape_string($id) . '", "' . $con->real_escape_string($amount) . '", "' . $con->real_escape_string($next_position_num) . '")';

			$final = mysqli_query($con, $sql) or redirect("Database Error");
			if($final){
				redirect("Item added");
			}
		}else{				
			// item is in the list - increase the amount
			$sql = 'UPDATE list SET list.amount = list.amount +  "' . $con->real_escape_string($amount) . '" WHERE list.item_id = "' . $con->real_escape_string($id) . '"';			
			$final = mysqli_query($con, $sql) or redirect("Database Error");
			if($final){
				redirect("Item added");
			}
		}
    }

    function loadRows() {
        global $con;                
        $sql = "SELECT * FROM list INNER JOIN items ON list.item_id=items.id ORDER BY list.position ASC" ;
        $result = mysqli_query($con, $sql) or redirect("Database Error");        
        return $result;
    }

    function loadDatalist() {
        global $con;                
        $sql = "SELECT * FROM items ORDER BY items.name ASC" ;
        $result = mysqli_query($con, $sql) or redirect("Error - ".mysqli_error($con));        
        return $result;
    }

    function redirect($message) {
        header("Location: index.php?message=" . urlencode($message));
    }

    // Handle the addition of new item
    if (isset($_POST['add_item']) and isset($_POST['add_amount'])) {
        $item = $_POST['add_item'];
        $amount = $_POST['add_amount'];
        if (validate_item($item) and validate_amount($amount)) {            
            storeRow(sanitize($item), sanitize($amount));
        } else {
            redirect("Invalid item");
        }
    }

include __DIR__ . '/templates/page.php';
?>