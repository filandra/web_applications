// Wait for the DOM to load
document.addEventListener("DOMContentLoaded", main);

class Tile {
  constructor(html_tile, id, x, y, valid) {
  	this.html_tile = html_tile
  	this.id = id;
    this.x = x;
    this.y = y;
    this.valid = valid;
  }
}

var id_tile_dict = {};
var tile_table = create_array(4)
var winnable = false

function main(){
	document.getElementById("game-tile-15").click()
	create_data()
	add_event_listeners();
	// Remove randomize() in order to check winnable combination
	randomize();	
	winnable = true;
	detect_win();
}

function create_data(){	
	var total = 1;
	var tile;
	for (var i = 0; i <= 3; i++) {
		for (var j = 0; j <= 3; j++) {			
			var id = "game-tile-"+ total.toString()
			tile = new Tile(document.getElementById(id), id, j, i, true);
			id_tile_dict[id] = tile;
			tile_table[j][i] = tile;
			total += 1;
		}	
	}

	tile_table[3][3] = new Tile(null, "", 3, 3, false);
	delete id_tile_dict["game-tile-16"];
}

function create_array(rows) {
  var arr = [];

  for (var i=0;i<rows;i++) {
     arr[i] = [];
  }

  return arr;
}

function add_event_listeners(){
	for (var i = 1; i <= 15; i++) {
		 document.getElementById("game-tile-"+ i.toString()).addEventListener("click", click_tile)
	}
}

function randomize(){	
	var num;
	for (var i = 0; i <= 1000; i++) {
		num = Math.floor((Math.random() * 15) + 1); 
		document.getElementById("game-tile-" + num.toString()).click();
	}		
}

function click_tile(){
	var clicked_tile = id_tile_dict[this.id];
	try_move(clicked_tile);
	detect_win();
}

function try_move(tile){
	var neighbour;
	// Look left
	if(tile.x - 1 >= 0){		
		neighbour = tile_table[tile.x-1][tile.y]
		if(neighbour.valid == false){
			swap(neighbour, tile)
			return;
		}
	}	

	// Look right
	if(tile.x + 1 <= 3){		
		neighbour = tile_table[tile.x+1][tile.y]		
		if(neighbour.valid == false){			
			swap(neighbour, tile)
			return;
		}
	}

	// Look up
	if(tile.y - 1 >= 0){
		neighbour = tile_table[tile.x][tile.y-1]
		if(neighbour.valid == false){
			swap(neighbour, tile)
			return;
		}
	}

	// Look down
	if(tile.y + 1 <= 3){
		neighbour = tile_table[tile.x][tile.y+1]
		if(neighbour.valid == false){
			swap(neighbour, tile)
			return;
		}
	}
}


function swap(neighbour, tile){
	swap_x = tile.x;
	swap_y = tile.y;

	tile.x = neighbour.x;
	tile.y = neighbour.y;

	neighbour.x = swap_x;
	neighbour.y = swap_y;

	tile.html_tile.style.left = tile.x*20+'vmin'
	tile.html_tile.style.top = tile.y*20+'vmin'

	tile_table[tile.x][tile.y] = tile
	tile_table[neighbour.x][neighbour.y] = neighbour

}

function detect_win(){

	if (winnable == false){
		return;
	}

	var item = 1;
	for (var i = 0; i <= 3; i++) {
		for (var j = 0; j <= 3; j++) {			
			if (item == 16) {continue;}	

			var tile = tile_table[j][i];					
			if (tile.valid == true && tile.html_tile.innerHTML == item.toString())
				item++;
			else{
				return;
			}
		}	
	}
	window.alert("Victory!")	
}