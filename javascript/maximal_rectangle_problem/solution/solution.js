function maxFreeRect(width, height, rects)
{	
	if(rects.length == 0){
		return {left: 0, top: 0, width: width, height: height}
	}else{
		var result = getMaxRect(width, height, rects);
		return {left: result[0], top: result[1], width: result[2], height: result[3]}	
	}
}

function updateHist(hist, rowIndex, interval, width, rects){
	// update the histogram according to the row we are interested in
	for (var columnIndex = 0; columnIndex < width; columnIndex++) {
		hist[columnIndex] += interval;
	}

	for (var rectIndex = 0; rectIndex < rects.length; rectIndex++) {
		drawRect(hist, rowIndex, rects[rectIndex])
	}	
}

function drawRect(hist, rowIndex, rect){
	// Fill the row of a rectangle into histogram
	if(rowIndex >= rect.top && rowIndex < rect.top + rect.height){
		hist.fill(0, rect.left, rect.left + rect.width)
	}
}

function getMaxRect(width, height, rects){
	var maxSize = [0,0,0];
	var startRow = 0;
	var maxArea = 0;
	var localArea = 0;

	var interestingRows = getInterestingRows(rects, height)
	var lastRow = -1;
	var hist = new Array(width).fill(0);	

	var localMax = [0,0,0];
	for (var i = 0; i < interestingRows.length; i++) {
		// Go through interesting rows, create histograms and search through them
		updateHist(hist, interestingRows[i], interestingRows[i] - lastRow, width, rects);
		lastRow = interestingRows[i];
		localMax = maxFromHistogram(hist);
		localArea = area(localMax);

		if(localArea > maxArea){
			maxSize = localMax;
			maxArea = localArea;
			startRow = interestingRows[i] + 1 - maxSize[0];
		}
	}
	return [maxSize[2], startRow, maxSize[1], maxSize[0]]
}

function getInterestingRows(rects, height){
	// We can skip a lot of rows - we know where the potential solutions are maximal
	var interestingRows = new Array();
	for (var i = 0; i < rects.length; i++) {
		//Just before a rectangle starts
		if(rects[i].top - 1 >= 0){
			interestingRows.push(rects[i].top - 1)
		}		

		// Just before rectangle ends
		if(rects[i].top + rects[i].height - 1 >= 0){
			interestingRows.push(rects[i].top + rects[i].height - 1)
		}		
	}
	// Add last row
	interestingRows.push(height-1)

	//Sort in order to get propper descend
	interestingRows.sort((a, b) => a - b);	

	return interestingRows;
}

// In order not to realloc every time
var stack = new Array();
var localMax = [0,0,0];
var pos = 10;
var start = 0;
var height = 0;
function maxFromHistogram(hist){
	// returns height, width, left
	this.stack = new Array();
	this.localMax = [0,0,0];	
	this.start = 0;
	this.height = 0;

	// Go through the histogram
	for (this.pos = 0; this.pos < hist.length; this.pos++){
		this.height = hist[this.pos];
		this.start = this.pos;
		while(true){
			// Can still add to new columns to the histogram
			if(this.stack.length == 0 || this.height > last()[1]){
				this.stack.push([this.start, this.height])	
			// Consume the histogram	
			}else if (this.stack.length > 0 && this.height < last()[1]){
				this.localMax = biggerRect(this.localMax, [last()[1], this.pos - last()[0], last()[0]])	
				this.start = this.stack.pop()[0]
				continue;
			}
			break;
		}
	}
	// Last consumation
	for(tuple in this.stack){
		this.localMax = biggerRect(this.localMax, [this.stack[tuple][1], this.pos - this.stack[tuple][0], this.stack[tuple][0]]) 
	}		
	return this.localMax;
}

function last(){
	return this.stack[this.stack.length - 1];
}

function area(shape){
	return shape[0] * shape[1]
}

function biggerRect(first, second){
	if (area([first[0], first[1]]) > area([second[0], second[1]])){
		return first;
	}else{
		return second;
	}
}

// In nodejs, this is the way how export is performed.
// In browser, module has to be a global varibale object.
module.exports = { maxFreeRect };
